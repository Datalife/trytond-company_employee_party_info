# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest
from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite


class CompanyEmployeePartyInfoTestCase(ModuleTestCase):
    """Test Company Employee Party Info module"""
    module = 'company_employee_party_info'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            CompanyEmployeePartyInfoTestCase))
    return suite
